import {
  Scene,
  PerspectiveCamera,
  WebGLRenderer
} from 'three';
import startAnimation from './animation';
import createObjectRegistry from './objects';

const DEFAULT_TARGET_ID = 'app';
const DEFAULT_RENDERER_OPTIONS = {
  antialias: true,
};

const createDefaultScene = () => new Scene();
const createDefaultCamera = ({ width, height }) => new PerspectiveCamera(50, width / height);
const createDefaultRenderer = (options) => new WebGLRenderer(options);
const getDefaultTarget = () => document.getElementById(DEFAULT_TARGET_ID);
const getElementDimensions = (el) => ({
  width: el.clientWidth,
  height: el.clientHeight,
});

export default function start(nextFn, options = {}) {
  const {
    createScene = createDefaultScene,
    createCamera = createDefaultCamera,
    createRenderer = createDefaultRenderer,
    target = getDefaultTarget(),
    rendererOptions = DEFAULT_RENDERER_OPTIONS,
  } = options;

  const dim = getElementDimensions(target);
  const scene = createScene();
  const camera = createCamera(dim);
  const renderer = createRenderer(rendererOptions);

  renderer.setSize(dim.width, dim.height);
  target.appendChild(renderer.domElement);

  const ctx = {
    scene,
    camera,
    renderer,
  }

  const animation = startAnimation(ctx);
  const objectRegistry = createObjectRegistry(scene, animation);

  const boot = {
    ...ctx,
    ...animation,
    ...objectRegistry,
  };

  nextFn(boot);
}
