class BootObject {
  constructor(props, onDestroy = () => {}) {
    if (!props) {
      throw new Error(`'props' expected for BootObject`);
    }
    if (!props.$create) {
      throw new Error(`'$create' expected for BootObject`);
    }

    Object.assign(this, props);

    try {
      this.$onDestroy = onDestroy;
      this.$onEnterFrame = this.$onEnterFrame && this.$onEnterFrame.bind(this);
      this.$mesh = this.$create && this.$create();
    } catch(e) {
      this.$destroy();
    }
  }

  $destroy() {
    if (this.$onDestroy) {
      this.$onDestroy(this);
    }
  }
}

export default function createObjectRegistry(scene, animation) {
  const removeObject = bootObj => {
    if (bootObj.$mesh) {
      scene.remove(bootObj.$mesh);
    }

    if (bootObj.$onEnterFrame) {
      animation.offEnterFrame(bootObj.$onEnterFrame);
    }
  }

  const addObject = props => {
    const bootObj = new BootObject(props, removeObject);

    if (bootObj.$mesh) {
      scene.add(bootObj.$mesh);
    }

    if (bootObj.$onEnterFrame) {
      animation.onEnterFrame(bootObj.$onEnterFrame);
    }
  };

  return {
    addObject,
    removeObject
  };
}
