import { Clock } from 'three';

export default function startAnimation({ renderer, scene, camera }) {
  const listeners = new Set();
  const clock = new Clock();

  function update(delta) {
    listeners.forEach(fn => fn(delta));
  }

  function render() {
    renderer.render(scene, camera);        
  }

  function animate() {
    const delta = clock.getDelta();

    update(delta);
    
    render();

    requestAnimationFrame(animate);
  }

  animate();
  
  return {
    onEnterFrame(fn) {
      listeners.add(fn);

      return () => {
        this.offEnterFrame(fn);
      };
    },
    offEnterFrame(fn) {
      listeners.delete(fn);
    },
  };
}
